package com.service.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestProjectGitApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestProjectGitApplication.class, args);
	}

}
