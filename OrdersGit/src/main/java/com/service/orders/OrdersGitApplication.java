package com.service.orders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrdersGitApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrdersGitApplication.class, args);
	}

}
